package me.snackmix.comboenchantsplitter;

import me.snackmix.comboenchantsplitter.commands.SplitCommand;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Objects;
import java.util.logging.Logger;

public class ComboEnchantSplitter extends JavaPlugin {
    public static ComboEnchantSplitter instance = null;
    public static Logger logger = null;
    public static Economy economy = null;
    private final PluginManager pluginManager = Bukkit.getPluginManager();

    public void onEnable() {
        instance = this;
        logger = getLogger();
        saveDefaultConfig();
        setupEconomy();
        getCommand("splitcombo").setExecutor(new SplitCommand());
    }

    private boolean setupEconomy() {
        if (pluginManager.getPlugin("Vault") == null) return false;
        RegisteredServiceProvider<Economy> rsp = Bukkit.getServicesManager().getRegistration(Economy.class);
        if (rsp == null) return false;
        economy = rsp.getProvider();
        return Objects.nonNull(economy);
    }

    public static boolean hasEconomy() {
        return Objects.nonNull(economy);
    }
}