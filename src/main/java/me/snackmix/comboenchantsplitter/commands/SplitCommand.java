package me.snackmix.comboenchantsplitter.commands;

import me.snackmix.comboenchantsplitter.ComboEnchantSplitter;
import me.snackmix.comboenchantsplitter.Experience;
import me.snackmix.comboenchantsplitter.PlayerUtils;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.EnchantmentStorageMeta;

import java.util.Arrays;
import java.util.List;
import java.util.function.BiConsumer;

public class SplitCommand implements CommandExecutor {
    public boolean onCommand(CommandSender sender, Command command, String label, String... args) {
        List<String> params = Arrays.asList(args);
        if (sender instanceof ConsoleCommandSender) {
            if (params.size() == 1) {
                if (params.get(0).equalsIgnoreCase("reload")) {
                    ComboEnchantSplitter.instance.reloadConfig();
                    sender.sendMessage("§aComboEnchantSplitter config has been reloaded.");
                }
            }
        }
        if (sender instanceof Player) {
            if (sender.hasPermission("combosplit.use")) {
                Player player = (Player) sender;
                ItemStack main = player.getInventory().getItemInMainHand();
                if (main.getItemMeta() instanceof EnchantmentStorageMeta) {
                    FileConfiguration config = ComboEnchantSplitter.instance.getConfig();
                    EnchantmentStorageMeta meta = (EnchantmentStorageMeta) main.getItemMeta();
                    int enchantStoreSize = meta.getStoredEnchants().size();
                    if (enchantStoreSize == 1) {
                        sender.sendMessage("§cCan only be used on books with more then one enchantment.");
                        return false;
                    }
                    if (ComboEnchantSplitter.hasEconomy()) {
                        if (config.getBoolean("use-price")) {
                            int cost = config.getInt("price-to-use");
                            if (ComboEnchantSplitter.economy.has(player, cost) == false) {
                                sender.sendMessage("§cYou need §e$" + cost + " §cto split this book.");
                                return false;
                            }
                        }
                    }
                    if (config.getBoolean("use-exp")) {
                        int cost = config.getInt("exp-to-use");
                        if (Experience.getExp(player) < cost) {
                            sender.sendMessage("§cYou need §e" + cost + " §cexperience points to split this book.");
                            return false;
                        }
                    }
                    meta.getStoredEnchants().forEach(new BiConsumer<Enchantment, Integer>() {
                        public void accept(Enchantment enchantment, Integer level) {
                            ItemStack item = new ItemStack(Material.ENCHANTED_BOOK, 1);
                            EnchantmentStorageMeta esm = (EnchantmentStorageMeta) item.getItemMeta();
                            esm.addStoredEnchant(enchantment, level, true);
                            item.setItemMeta(esm);
                            PlayerUtils.addItems(player, item);
                        }
                    });
                    if (ComboEnchantSplitter.hasEconomy()) {
                        if (config.getBoolean("use-price")) {
                            int cost = config.getInt("price-to-use");
                            ComboEnchantSplitter.economy.withdrawPlayer(player, cost);
                        }
                    }
                    if (config.getBoolean("use-exp")) {
                        Experience.changeExp(player, -config.getInt("exp-to-use"));
                    }
                    PlayerUtils.playSound(player, Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    sender.sendMessage("§6You split the book into §c" + enchantStoreSize + " §6different enchantment books.");
                    player.getInventory().setItemInMainHand(null);
                }
            } else {
                sender.sendMessage("§4You do not have access to that command.");
            }
        }
        return false;
    }
}